# CinemaPoster

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**cinema** | [**\Swagger\Client\Model\Cinema**](Cinema.md) |  | [optional] 
**poster** | [**\Swagger\Client\Model\Poster**](Poster.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


