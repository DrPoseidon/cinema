# Swagger\Client\CommentApi

All URIs are relative to *https://virtserver.swaggerhub.com/a.Popov.d/Online_Cinema/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**commentCreate**](CommentApi.md#commentCreate) | **POST** /comment | Add a new comment
[**commentDel**](CommentApi.md#commentDel) | **DELETE** /comment/{commentId} | Deletes a poster
[**commentGet**](CommentApi.md#commentGet) | **GET** /comment | Returns all comment


# **commentCreate**
> commentCreate($body)

Add a new comment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CommentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\Comment(); // \Swagger\Client\Model\Comment | Comment object that needs to be added

try {
    $apiInstance->commentCreate($body);
} catch (Exception $e) {
    echo 'Exception when calling CommentApi->commentCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Comment**](../Model/Comment.md)| Comment object that needs to be added |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commentDel**
> commentDel($comment_id, $api_key)

Deletes a poster

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CommentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$comment_id = 789; // int | Pet id to delete
$api_key = "api_key_example"; // string | 

try {
    $apiInstance->commentDel($comment_id, $api_key);
} catch (Exception $e) {
    echo 'Exception when calling CommentApi->commentDel: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **comment_id** | **int**| Pet id to delete |
 **api_key** | **string**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commentGet**
> \Swagger\Client\Model\Comment[] commentGet($poster)

Returns all comment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CommentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$poster = new \Swagger\Client\Model\Poster(); // \Swagger\Client\Model\Poster | 

try {
    $result = $apiInstance->commentGet($poster);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommentApi->commentGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **poster** | [**\Swagger\Client\Model\Poster**](../Model/Poster.md)|  |

### Return type

[**\Swagger\Client\Model\Comment[]**](../Model/Comment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

