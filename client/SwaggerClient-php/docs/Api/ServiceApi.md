# Swagger\Client\ServiceApi

All URIs are relative to *https://virtserver.swaggerhub.com/a.Popov.d/Online_Cinema/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**service**](ServiceApi.md#service) | **GET** /service/{serviceId} | Returns all service
[**serviceChange**](ServiceApi.md#serviceChange) | **PUT** /service/change | Returns all service
[**serviceGet**](ServiceApi.md#serviceGet) | **GET** /service | Returns all service


# **service**
> \Swagger\Client\Model\Comment[] service($service_id)

Returns all service

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ServiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$service_id = 789; // int | 

try {
    $result = $apiInstance->service($service_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiceApi->service: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_id** | **int**|  |

### Return type

[**\Swagger\Client\Model\Comment[]**](../Model/Comment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **serviceChange**
> \Swagger\Client\Model\Service[] serviceChange($body)

Returns all service

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ServiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array(new \Swagger\Client\Model\Service()); // \Swagger\Client\Model\Service[] | 

try {
    $result = $apiInstance->serviceChange($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiceApi->serviceChange: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Service[]**](../Model/Service.md)|  |

### Return type

[**\Swagger\Client\Model\Service[]**](../Model/Service.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **serviceGet**
> \Swagger\Client\Model\Service[] serviceGet()

Returns all service

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ServiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->serviceGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiceApi->serviceGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\Service[]**](../Model/Service.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

