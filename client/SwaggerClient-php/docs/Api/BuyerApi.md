# Swagger\Client\BuyerApi

All URIs are relative to *https://virtserver.swaggerhub.com/a.Popov.d/Online_Cinema/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cardSet**](BuyerApi.md#cardSet) | **POST** /buyer/card | Set card
[**cinemaGet**](BuyerApi.md#cinemaGet) | **GET** /buyer/cinema | Returns all cinema
[**placeGet**](BuyerApi.md#placeGet) | **GET** /buyer/place | Returns all place
[**ticketPayment**](BuyerApi.md#ticketPayment) | **POST** /buyer/ticket | ticket payment


# **cardSet**
> \Swagger\Client\Model\Card cardSet($card)

Set card

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BuyerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$card = new \Swagger\Client\Model\Card(); // \Swagger\Client\Model\Card | 

try {
    $result = $apiInstance->cardSet($card);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuyerApi->cardSet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **card** | [**\Swagger\Client\Model\Card**](../Model/Card.md)|  |

### Return type

[**\Swagger\Client\Model\Card**](../Model/Card.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cinemaGet**
> \Swagger\Client\Model\Cinema[] cinemaGet($poster)

Returns all cinema

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BuyerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$poster = new \Swagger\Client\Model\Poster(); // \Swagger\Client\Model\Poster | 

try {
    $result = $apiInstance->cinemaGet($poster);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuyerApi->cinemaGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **poster** | [**\Swagger\Client\Model\Poster**](../Model/Poster.md)|  |

### Return type

[**\Swagger\Client\Model\Cinema[]**](../Model/Cinema.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **placeGet**
> \Swagger\Client\Model\Place[] placeGet($cinema_poster)

Returns all place

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BuyerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cinema_poster = new \Swagger\Client\Model\CinemaPoster(); // \Swagger\Client\Model\CinemaPoster | 

try {
    $result = $apiInstance->placeGet($cinema_poster);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuyerApi->placeGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cinema_poster** | [**\Swagger\Client\Model\CinemaPoster**](../Model/CinemaPoster.md)|  |

### Return type

[**\Swagger\Client\Model\Place[]**](../Model/Place.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketPayment**
> \Swagger\Client\Model\Ticket ticketPayment($ticket)

ticket payment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BuyerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ticket = new \Swagger\Client\Model\Ticket(); // \Swagger\Client\Model\Ticket | 

try {
    $result = $apiInstance->ticketPayment($ticket);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BuyerApi->ticketPayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticket** | [**\Swagger\Client\Model\Ticket**](../Model/Ticket.md)|  |

### Return type

[**\Swagger\Client\Model\Ticket**](../Model/Ticket.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

