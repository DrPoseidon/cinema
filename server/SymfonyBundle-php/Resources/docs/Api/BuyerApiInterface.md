# Swagger\Server\Api\BuyerApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/a.Popov.d/Online_Cinema/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cardSet**](BuyerApiInterface.md#cardSet) | **POST** /buyer/card | Set card
[**cinemaGet**](BuyerApiInterface.md#cinemaGet) | **GET** /buyer/cinema | Returns all cinema
[**placeGet**](BuyerApiInterface.md#placeGet) | **GET** /buyer/place | Returns all place
[**ticketPayment**](BuyerApiInterface.md#ticketPayment) | **POST** /buyer/ticket | ticket payment


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.buyer:
        class: Acme\MyBundle\Api\BuyerApi
        tags:
            - { name: "swagger_server.api", api: "buyer" }
    # ...
```

## **cardSet**
> Swagger\Server\Model\Card cardSet($card)

Set card

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/BuyerApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\BuyerApiInterface;

class BuyerApi implements BuyerApiInterface
{

    // ...

    /**
     * Implementation of BuyerApiInterface#cardSet
     */
    public function cardSet(Card $card)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **card** | [**Swagger\Server\Model\Card**](../Model/Card.md)|  |

### Return type

[**Swagger\Server\Model\Card**](../Model/Card.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **cinemaGet**
> Swagger\Server\Model\Cinema cinemaGet($poster)

Returns all cinema

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/BuyerApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\BuyerApiInterface;

class BuyerApi implements BuyerApiInterface
{

    // ...

    /**
     * Implementation of BuyerApiInterface#cinemaGet
     */
    public function cinemaGet(Poster $poster)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **poster** | [**Swagger\Server\Model\Poster**](../Model/Poster.md)|  |

### Return type

[**Swagger\Server\Model\Cinema**](../Model/Cinema.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **placeGet**
> Swagger\Server\Model\Place placeGet($cinemaPoster)

Returns all place

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/BuyerApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\BuyerApiInterface;

class BuyerApi implements BuyerApiInterface
{

    // ...

    /**
     * Implementation of BuyerApiInterface#placeGet
     */
    public function placeGet(CinemaPoster $cinemaPoster)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cinemaPoster** | [**Swagger\Server\Model\CinemaPoster**](../Model/CinemaPoster.md)|  |

### Return type

[**Swagger\Server\Model\Place**](../Model/Place.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **ticketPayment**
> Swagger\Server\Model\Ticket ticketPayment($ticket)

ticket payment

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/BuyerApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\BuyerApiInterface;

class BuyerApi implements BuyerApiInterface
{

    // ...

    /**
     * Implementation of BuyerApiInterface#ticketPayment
     */
    public function ticketPayment(Ticket $ticket)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticket** | [**Swagger\Server\Model\Ticket**](../Model/Ticket.md)|  |

### Return type

[**Swagger\Server\Model\Ticket**](../Model/Ticket.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

