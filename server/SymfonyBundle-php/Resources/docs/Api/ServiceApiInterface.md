# Swagger\Server\Api\ServiceApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/a.Popov.d/Online_Cinema/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**service**](ServiceApiInterface.md#service) | **GET** /service/{serviceId} | Returns all service
[**serviceChange**](ServiceApiInterface.md#serviceChange) | **PUT** /service/change | Returns all service
[**serviceGet**](ServiceApiInterface.md#serviceGet) | **GET** /service | Returns all service


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.service:
        class: Acme\MyBundle\Api\ServiceApi
        tags:
            - { name: "swagger_server.api", api: "service" }
    # ...
```

## **service**
> Swagger\Server\Model\Comment service($serviceId)

Returns all service

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ServiceApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ServiceApiInterface;

class ServiceApi implements ServiceApiInterface
{

    // ...

    /**
     * Implementation of ServiceApiInterface#service
     */
    public function service($serviceId)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serviceId** | **int**|  |

### Return type

[**Swagger\Server\Model\Comment**](../Model/Comment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **serviceChange**
> Swagger\Server\Model\Service serviceChange($body)

Returns all service

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ServiceApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ServiceApiInterface;

class ServiceApi implements ServiceApiInterface
{

    // ...

    /**
     * Implementation of ServiceApiInterface#serviceChange
     */
    public function serviceChange(array $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\Service**](../Model/Service.md)|  |

### Return type

[**Swagger\Server\Model\Service**](../Model/Service.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **serviceGet**
> Swagger\Server\Model\Service serviceGet()

Returns all service

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ServiceApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\ServiceApiInterface;

class ServiceApi implements ServiceApiInterface
{

    // ...

    /**
     * Implementation of ServiceApiInterface#serviceGet
     */
    public function serviceGet()
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Swagger\Server\Model\Service**](../Model/Service.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

