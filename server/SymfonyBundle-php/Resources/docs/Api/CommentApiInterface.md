# Swagger\Server\Api\CommentApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/a.Popov.d/Online_Cinema/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**commentCreate**](CommentApiInterface.md#commentCreate) | **POST** /comment | Add a new comment
[**commentDel**](CommentApiInterface.md#commentDel) | **DELETE** /comment/{commentId} | Deletes a poster
[**commentGet**](CommentApiInterface.md#commentGet) | **GET** /comment | Returns all comment


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.comment:
        class: Acme\MyBundle\Api\CommentApi
        tags:
            - { name: "swagger_server.api", api: "comment" }
    # ...
```

## **commentCreate**
> commentCreate($body)

Add a new comment

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/CommentApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\CommentApiInterface;

class CommentApi implements CommentApiInterface
{

    // ...

    /**
     * Implementation of CommentApiInterface#commentCreate
     */
    public function commentCreate(Comment $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\Comment**](../Model/Comment.md)| Comment object that needs to be added |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **commentDel**
> commentDel($commentId, $apiKey)

Deletes a poster

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/CommentApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\CommentApiInterface;

class CommentApi implements CommentApiInterface
{

    // ...

    /**
     * Implementation of CommentApiInterface#commentDel
     */
    public function commentDel($commentId, $apiKey = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **commentId** | **int**| Pet id to delete |
 **apiKey** | **string**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **commentGet**
> Swagger\Server\Model\Comment commentGet($poster)

Returns all comment

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/CommentApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\CommentApiInterface;

class CommentApi implements CommentApiInterface
{

    // ...

    /**
     * Implementation of CommentApiInterface#commentGet
     */
    public function commentGet(Poster $poster)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **poster** | [**Swagger\Server\Model\Poster**](../Model/Poster.md)|  |

### Return type

[**Swagger\Server\Model\Comment**](../Model/Comment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

