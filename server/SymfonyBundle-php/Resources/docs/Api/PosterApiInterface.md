# Swagger\Server\Api\PosterApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/a.Popov.d/Online_Cinema/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**posterCreate**](PosterApiInterface.md#posterCreate) | **POST** /poster | Create new poster
[**posterDel**](PosterApiInterface.md#posterDel) | **DELETE** /poster/{posterId} | Deletes a poster
[**posterGetById**](PosterApiInterface.md#posterGetById) | **GET** /poster/{posterId} | Get poster by ID
[**posterUpdate**](PosterApiInterface.md#posterUpdate) | **PUT** /poster | Update an existing poster
[**posterUpdateTrailer**](PosterApiInterface.md#posterUpdateTrailer) | **PUT** /poster/change_trailer | Update an existing poster
[**postersGet**](PosterApiInterface.md#postersGet) | **GET** /poster | Returns all posters


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.poster:
        class: Acme\MyBundle\Api\PosterApi
        tags:
            - { name: "swagger_server.api", api: "poster" }
    # ...
```

## **posterCreate**
> posterCreate($body)

Create new poster

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/PosterApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\PosterApiInterface;

class PosterApi implements PosterApiInterface
{

    /**
     * Configure OAuth2 access token for authorization: petstore_auth
     */
    public function setpetstore_auth($oauthToken)
    {
        // Retrieve logged in user from $oauthToken ...
    }

    // ...

    /**
     * Implementation of PosterApiInterface#posterCreate
     */
    public function posterCreate(Poster $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\Poster**](../Model/Poster.md)| Created poster object |

### Return type

void (empty response body)

### Authorization

[petstore_auth](../../README.md#petstore_auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **posterDel**
> posterDel($posterId, $apiKey)

Deletes a poster

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/PosterApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\PosterApiInterface;

class PosterApi implements PosterApiInterface
{

    // ...

    /**
     * Implementation of PosterApiInterface#posterDel
     */
    public function posterDel($posterId, $apiKey = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posterId** | **int**| Pet id to delete |
 **apiKey** | **string**|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **posterGetById**
> Swagger\Server\Model\Poster posterGetById($posterId)

Get poster by ID

Returns a single poster

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/PosterApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\PosterApiInterface;

class PosterApi implements PosterApiInterface
{

    // ...

    /**
     * Implementation of PosterApiInterface#posterGetById
     */
    public function posterGetById($posterId)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posterId** | **int**| ID of poster to return |

### Return type

[**Swagger\Server\Model\Poster**](../Model/Poster.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **posterUpdate**
> posterUpdate($body)

Update an existing poster

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/PosterApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\PosterApiInterface;

class PosterApi implements PosterApiInterface
{

    /**
     * Configure OAuth2 access token for authorization: petstore_auth
     */
    public function setpetstore_auth($oauthToken)
    {
        // Retrieve logged in user from $oauthToken ...
    }

    // ...

    /**
     * Implementation of PosterApiInterface#posterUpdate
     */
    public function posterUpdate(Poster $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Swagger\Server\Model\Poster**](../Model/Poster.md)| Poster object that needs to be added to the store |

### Return type

void (empty response body)

### Authorization

[petstore_auth](../../README.md#petstore_auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **posterUpdateTrailer**
> posterUpdateTrailer($trailer)

Update an existing poster

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/PosterApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\PosterApiInterface;

class PosterApi implements PosterApiInterface
{

    /**
     * Configure OAuth2 access token for authorization: petstore_auth
     */
    public function setpetstore_auth($oauthToken)
    {
        // Retrieve logged in user from $oauthToken ...
    }

    // ...

    /**
     * Implementation of PosterApiInterface#posterUpdateTrailer
     */
    public function posterUpdateTrailer($trailer)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **trailer** | **string**|  |

### Return type

void (empty response body)

### Authorization

[petstore_auth](../../README.md#petstore_auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **postersGet**
> Swagger\Server\Model\Poster postersGet()

Returns all posters

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/PosterApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\PosterApiInterface;

class PosterApi implements PosterApiInterface
{

    /**
     * Configure API key authorization: api_key
     */
    public function setapi_key($apiKey)
    {
        // Retrieve logged in user from $apiKey ...
    }

    // ...

    /**
     * Implementation of PosterApiInterface#postersGet
     */
    public function postersGet()
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Swagger\Server\Model\Poster**](../Model/Poster.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

